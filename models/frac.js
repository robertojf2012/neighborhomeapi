'use strict'
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FracSchema = Schema({
	strNombre: String,
	strDescripcion: String,
	strLogo: String,
	intLatitud: Number,
	intLongitud: Number,
	cuotas: Array,
	casas: Array,
	noticias: Array
})

module.exports = mongoose.model('Fraccionamiento',FracSchema);