'use strict'
const mongoose = require('mongoose');
const Schema = mongoose.Schema, ObjectId = Schema.ObjectId;

const UsuarioSchema = Schema({
	idFrac: ObjectId,
	intNoCasa: Number,
	strEmail: String,
	strPassword: String,
	strNombre: String,
	strApat: String,
	strAmat: String,
	intTelefono: Number,
	boolAdmin: Boolean,
	pagos: Array
})

module.exports = mongoose.model('Usuario',UsuarioSchema);