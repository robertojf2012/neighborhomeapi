'use strict'
var path = require("path");
const Usuario = require('../models/user');
const Fraccionamiento = require('../models/frac');

function logIn(req,res){
	var email = req.body.email;
	var password = req.body.password;

	Usuario.findOne({strEmail:email, strPassword:password},function(err,user){
		
		if(err){
			console.log(err);
			return res.status(500).send();
		}

		if(!user){
			return res.status(404).send();
		}

		req.session.user = user
		return res.status(200).send(user);
	
	})
}

function getFrac(req,res){
	
	var idFrac = req.params.idFrac;

	/*
	if(!req.session.user){
		return res.status(401).send();
	}
	*/

	Fraccionamiento.findById(idFrac,function(err,frac){
		
		if(err){
			console.log(err);
			return res.status(500).send();
		}

		if(!frac){
			return res.status(404).send();
		}

		return res.status(200).send(frac);

	})
}

function getFrac2(req,res){
	
	var idFrac = req.params.idFrac;

	Fraccionamiento.findById(idFrac,function(err,frac){
		
		if(err){
			console.log(err);
			return res.status(500).send();
		}

		if(!frac){
			return res.status(404).send();
		}

		return res.status(200).send({"frac":frac});

	})
}

function getUsuarios(req,res){

	var idFrac = req.params.idFrac;

	Usuario.find({'idFrac':idFrac}).collation({locale:"en"}).exec((err,users)=>{
		
		/*
		if(!req.session.user){
			return res.status(401).send();
		}
		*/

		if(err){
			return res.status(500).send();
		}
		if(!users){
			return res.status(404).send();
		}

		return res.status(200).send(users);

	});
}

function getUsuarios2(req,res){

	var idFrac = req.params.idFrac;

	Usuario.find({'idFrac':idFrac}).collation({locale:"en"}).exec((err,users)=>{

		if(err){
			return res.status(500).send();
		}
		if(!users){
			return res.status(404).send();
		}

		return res.status(200).send({"users":users});

	});
}

function postUser(req,res){
	
	var usuario = new Usuario();
	var params = req.body;
	
	usuario.idFrac = params.idFrac;
	usuario.intNoCasa = params.intNoCasa;
	usuario.strEmail = params.strEmail;
	usuario.strPassword = params.strPassword;
	usuario.strNombre = params.strNombre;
	usuario.strApat = params.strApat;
	usuario.strAmat = params.strAmat;
	usuario.intTelefono = params.intTelefono;
	usuario.boolAdmin = params.boolAdmin;
	usuario.pagos = params.pagos;

	usuario.save((err,usuarioSaved)=>{
		if(err){
			return res.status(500).send();	
		}else{
			return res.status(200).send();
		}
	});

}

function setCasa(req,res){

	var idFrac = req.params.idFrac;
	var data = req.body;

	Fraccionamiento.findByIdAndUpdate(idFrac,{$push:{"casas": data}},function(err,casaSaved){
		if(err){
			res.status(500).send();
		}else{
			res.status(200).send();
		}

	})
}

function logOut(req,res){
	req.session.destroy();
	return res.status(200).send();
}

function checkSession(req,res){
	if(!req.session.user){
		return res.status(401).send();
	}
	return res.status(200).send();
}


module.exports = {
	logIn,
	logOut,
	checkSession,
	getFrac,
	getFrac2,
	getUsuarios,
	getUsuarios2,
	postUser,
	setCasa
}