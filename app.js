'use strict' 
const express = require('express'); //cargando express
const bodyParser = require('body-parser'); //cargando body-parser
const session = require('express-session'); //Para LOGIN
var path = require("path");
const app = express();
const api = require('./routes/neighborh');
var cors = require('cors');

app.use(express.static(__dirname + '/node_modules/bootstrap/dist'));

app.use(express.static(__dirname + '/views'));

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(session({secret:"879622dsfyeanSuperSecret777",resave:false, saveUninitialized:true}));

/*
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
	next();
});
*/

var corsHeaders = {
  origin: true, //http://localhost:4200  origin: ["*"], //http://localhost:4200
  credentials: true
};


app.use(cors(corsHeaders));

app.use('/',api);

module.exports = app;