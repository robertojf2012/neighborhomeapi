'use strict'
const express = require('express');
const api = express.Router();
const NeighborController = require('../controllers/neighborh'); 

//GETS
api.get('/api/session',NeighborController.checkSession);
api.get('/api/logout',NeighborController.logOut);
api.get('/api/frac/:idFrac',NeighborController.getFrac);
api.get('/api/fracmobile/:idFrac',NeighborController.getFrac2);
api.get('/api/users/:idFrac',NeighborController.getUsuarios);
api.get('/api/usersmobile/:idFrac',NeighborController.getUsuarios2);


//POSTS
api.post('/api/login',NeighborController.logIn);
api.post('/api/newUser',NeighborController.postUser);

//UPDATES
api.put('/api/setCasa/:idFrac',NeighborController.setCasa);





module.exports = api;